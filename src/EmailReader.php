<?php

declare(strict_types=1);

namespace EmailReader;

use DateTime;

class EmailReader {

    private Connect $connect;

    /**
     * @param string $host - host string (inject from config)
     * @param string $user - username (inject from config)
     * @param string $pass - password (inject from config)
     * @param array $parserRegexes - list of regexes to parse body (inject from config)
     * @throws EmailReaderException
     */
    public function __construct(
        private readonly string $host,
        private readonly string $user,
        private readonly string $pass,
        private readonly array $parserRegexes = [],
        private readonly int $fromTrasholdSeconds
    ) {
        $this->connect = new Connect($this->host, $this->user, $this->pass);
    }

    /**
     * @param EmailReaderParams $params - options
     * @return MailObjectList - list of email objects
     */
    public function process(EmailReaderParams $params) : MailObjectList {

        // Just move from a bit earlier
        if ($params->from !== null) {
            $fromWithTrashold = $params->from;
            $fromWithTrashold->modify("-$this->fromTrasholdSeconds seconds");
            $params->from = $fromWithTrashold;
        }

        // Get emails
        $filteredEmailIdsArray = [];
        foreach ($params->possibleSubjectList as $subject) {

            // Parse criteria
            $criteria = "SUBJECT \"$subject\"";
            if ($params->from !== null) {
                $date = $params->from->format("d M Y");
                $criteria .= "SINCE \"$date\"";
            }

            $filteredEmailIds = imap_search($this->connect->connection, $criteria);
            if ($filteredEmailIds !== false) { $filteredEmailIdsArray = array_merge($filteredEmailIdsArray, $filteredEmailIds); }
        }

        rsort($filteredEmailIdsArray);
        return new MailObjectList(
            connect: $this->connect,
            emailIdList: $filteredEmailIdsArray,
            parserRegexes: $this->parserRegexes,
            from: $params->from,
            fullText: $params->fullText,
        );
    }

    /**
     * @param int $runSeconds - run another cycle if not reached checking time
     * @param int $oneRoundSeconds - if one cycle is faster, add timeout (throttling)
     * @param EmailReaderParams $params - email reader options
     * @return MailObjectList|null
     * @noinspection PhpUnused
     */
    public function loopCheck(int $runSeconds, int $oneRoundSeconds, EmailReaderParams $params) : MailObjectList|null {

        $startTimeStamp = (new DateTime())->getTimestamp();

        do {

            // Run the process
            $runTimeStamp = (new DateTime())->getTimestamp();
            $result = $this->process($params);
            $processRunTimeSeconds = (new DateTime())->getTimestamp() - $runTimeStamp;

            // Extra handling
            if (count($result->list) > 0) { return $result; }
            if ($processRunTimeSeconds < $oneRoundSeconds) {
                sleep($oneRoundSeconds - $processRunTimeSeconds);
            }

            // Total run diff
            $runSecondsDiff = (new DateTime())->getTimestamp() - $startTimeStamp;

        } while ($runSecondsDiff < $runSeconds);

        return null;
    }
}