<?php

declare(strict_types=1);

namespace EmailReader;

use DateTime;

class MailObject {

    /** @var string - mail subject */
    public string $subject;

    /** @var string - mail raw or parsed body */
    public string $body;

    /** @var DateTime - mail datetime */
    public DateTime $date;

    /** @var string - use to separate parsed matches */
    public static string $separator = "|";

    /**
     * @param Connect $connect - connection object
     * @param int $id - email ID
     * @param array[] $parserRegexes - list of regexes to use for parsing
     * @noinspection PhpPropertyOnlyWrittenInspection
     */
    public function __construct(Connect $connect, private readonly int $id, array $parserRegexes) {

        // Load email info
        $header_from_body = imap_fetchbody($connect->connection, $id, '0');
        $overview = imap_rfc822_parse_headers($header_from_body);
        $bodyRaw = imap_body($connect->connection,$id);
        $this->subject = join('', array_map(static function(\stdClass $fragment) { return $fragment->text; }, imap_mime_header_decode($overview->subject)));
        $this->body = $bodyRaw === false ? '' : strip_tags(quoted_printable_decode($bodyRaw));

        // Parse date
        $this->date = new DateTime();
        $this->date->setTimestamp(strtotime($overview->date));

        // Try to parse the content
        if (strlen(trim($this->body)) === 0) { return; }
        foreach ($parserRegexes as $regexPair) {

            $regexSubject = array_keys($regexPair)[0];
            $regexExpression = array_values($regexPair)[0];

            // Only if subject of config matches
            if ($regexSubject === $this->subject) {

                // Get first match, shift raw data, join with |
                preg_match_all($regexExpression, $this->body, $matches, PREG_SET_ORDER);
                if (count($matches) !== 1) { continue; }
                $match = $matches[0];
                array_shift($match);
                $this->body = join(self::$separator, $match);
            }
        }
    }
}