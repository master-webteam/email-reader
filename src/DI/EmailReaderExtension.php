<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace EmailReader\DI;

use EmailReader\EmailReader;
use Nette\DI\CompilerExtension;

class EmailReaderExtension extends CompilerExtension {

    public function loadConfiguration(): void {

        $config = $this->getConfig();
        $builder = $this->getContainerBuilder();

        $builder
            ->addDefinition($this->prefix('emailReader'))
            ->setFactory(
                factory: EmailReader::class,
                args: [
                    $config['host'],
                    $config['user'],
                    $config['pass'],
                    $config['parserRegexes'] ?? [],
                    $config['fromTrasholdSeconds'] ?? 5
                ]
            );
    }
}