<?php

declare(strict_types=1);

namespace EmailReader;

class Connect {

    /** @var false|resource */
    public $connection;

    /**
     * @param string $host
     * @param string $user
     * @param string $pass
     * @throws EmailReaderException
     */
    public function __construct(string $host, string $user, string $pass) {

        $this->connection = imap_open($host, $user, $pass);
        if (!$this->connection) { throw new EmailReaderException(message: 'Impossible to open connection'); }
    }

    /**
     * @throws EmailReaderException
     */
    public function __destruct() {
        if (!imap_close($this->connection)) {
            throw new EmailReaderException(message: 'Impossible to close connection');
        }
    }
}