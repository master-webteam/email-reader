<?php

declare(strict_types=1);

namespace EmailReader;

use Exception;

class EmailReaderException extends Exception {}