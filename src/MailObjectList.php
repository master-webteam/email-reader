<?php

declare(strict_types=1);

namespace EmailReader;

use DateTime;

class MailObjectList {

    /** @var MailObject[] */
    public array $list;

    /**
     * @param Connect $connect - connection object
     * @param int[] $emailIdList - email IDs list
     * @param array[] $parserRegexes - regexes to parse body
     * @param DateTime|null $from - filter from date
     * @param string|null $fullText - search email body (if parsed, parsed)
     */
    public function __construct(Connect $connect, array $emailIdList, array $parserRegexes, DateTime|null $from, string|null $fullText) {

        $this->list =
            array_values(
                array_filter(
                    array_map(static function(int $emailId) use ($connect, $parserRegexes) {
                        return new MailObject($connect, $emailId, $parserRegexes);
                    }, $emailIdList),
                    static function (MailObject $mail) use ($from, $fullText) {
                        return strlen(trim($mail->body)) > 0
                            && ($fullText === null || str_contains($mail->body, $fullText))
                            && ($from === null || $mail->date->getTimestamp() >= $from->getTimestamp());
                    }
                )
            );
    }
}