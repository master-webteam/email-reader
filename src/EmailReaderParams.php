<?php

namespace EmailReader;

use DateTime;

class EmailReaderParams {

    /**
     * @param string[] $possibleSubjectList - filter any of subjects
     * @param DateTime|null $from - filter mail date from
     * @param string|null $fullText - fulltext search email body (if parsed, parsed)
     */
    public function __construct(
        public array $possibleSubjectList,
        public ?DateTime $from = null,
        public ?string $fullText = null
    ) {}
}